package android.dhartinews.Home.ArticleFragment;

import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ArticleDetailActivity extends AppCompatActivity {


    private TextView txtDetailTitle;
    private TextView txtDetailDescription;
    private ImageView imgDetailArticle;

    private String id = "";
    private String imageUrl = "";
    private String title = "";
    private String description = "";
    private String userType = "";
    private String userName = "";

    private String articalApprovedUrl = Url.SERVER_URL + "/dn/update/update_article.php";
    private String uploadCommentUrl = Url.SERVER_URL + "/dn/insert/insert_comment.php";
    private String viewCommentUrl = Url.SERVER_URL + "/dn/view/view_comment.php";

    private HashMap<String, String> data;
    private WsManager wsManager;
    private Button btnApprovedArticle, btnUploadComment;
    private EditText editComment;

    private RecyclerView recyclerViewComment;
    private CommentAdapterClass adapterClass;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ArticleDataClass> commentAdapterClassArrayList;

    private ArticleDataClass articleDataClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtDetailTitle = (TextView) findViewById(R.id.txtDetailsArticleTitle);
        txtDetailDescription = (TextView) findViewById(R.id.txtDetailsArticleDescription);
        imgDetailArticle = (ImageView) findViewById(R.id.imgDetailArticle);
        btnApprovedArticle = (Button) findViewById(R.id.btnApprovedArticel);
        btnUploadComment = (Button) findViewById(R.id.btnUploadComment);
        editComment = (EditText) findViewById(R.id.editArticleComment);

        recyclerViewComment = (RecyclerView) findViewById(R.id.recyclerViewCommment);
        commentAdapterClassArrayList = new ArrayList<>();


        wsManager = new WsManager(this);
        data = new HashMap<>();

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        imageUrl = intent.getStringExtra("image");
        title = intent.getStringExtra("title");
        description = intent.getStringExtra("description");
        userType = intent.getStringExtra("USER_TYPE");
        userName = intent.getStringExtra("USER_NAME");
        populateArticle();


        data.put("id", id);
        //data.put("status", "true");

        if (userType.equals("admin")) {

            btnApprovedArticle.setVisibility(View.VISIBLE);
            btnUploadComment.setVisibility(View.INVISIBLE);
            recyclerViewComment.setVisibility(View.INVISIBLE);
            btnApprovedArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();

                    wsManager.postAsMap(data, articalApprovedUrl, new WSResponse() {
                        @Override
                        public void onError(String error) {

                            super.onError(error);
                            Log.d("error", error.toString());
                            Toast.makeText(ArticleDetailActivity.this, "Problem in approveing article", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            Log.d("response", data.toString());
                            Toast.makeText(ArticleDetailActivity.this, "Article Approved Successfully", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                }
            });
        }

        if (userType.equals("user")) {

            btnApprovedArticle.setVisibility(View.INVISIBLE);
            btnUploadComment.setVisibility(View.VISIBLE);
            recyclerViewComment.setVisibility(View.VISIBLE);


            btnUploadComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String comment = editComment.getText().toString();
                    data.clear();
                    data.put("articleId", id);
                    data.put("comment", comment);
                    data.put("userName", userName);

                    wsManager.postAsMap(data, uploadCommentUrl, new WSResponse() {
                        @Override
                        public void onError(String error) {

                            super.onError(error);
                            Log.d("error", error.toString());
                            Toast.makeText(ArticleDetailActivity.this, "Problem in uploading comment", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            Log.d("response", data.toString());
                            Toast.makeText(ArticleDetailActivity.this, "Comment Upload Successfully", Toast.LENGTH_LONG).show();
                            editComment.setText("");
                            loadComment();
// finish();
                        }
                    });
                }
            });

            loadComment();

        }
    }


    private void loadComment() {
        data.clear();
        // JSONObject object = new JSONObject();
        data.put("articleId", id);
        wsManager.postAsMap(data, viewCommentUrl, new WSResponse() {
            @Override
            public void onSuccess(JSONObject data) {

                if (data != null) {
                    commentAdapterClassArrayList.clear();
                    JSONArray array = null;
                    try {

                        array = data.getJSONArray("comment");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject comment = array.getJSONObject(i);
                            articleDataClass = new ArticleDataClass();
                            articleDataClass.setComment(comment);
                            commentAdapterClassArrayList.add(articleDataClass);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    linearLayoutManager = new LinearLayoutManager(ArticleDetailActivity.this);
                    adapterClass = new CommentAdapterClass(ArticleDetailActivity.this,commentAdapterClassArrayList);
                    recyclerViewComment.setLayoutManager(linearLayoutManager);
                    recyclerViewComment.setAdapter(adapterClass);



                }

            }
        });
    }

    private void populateArticle() {

        WsManager.loadImage(this, imageUrl, imgDetailArticle);
        txtDetailTitle.setText(title);
        txtDetailDescription.setText(description);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
