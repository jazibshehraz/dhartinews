package android.dhartinews.Home.ArticleFragment;

import android.content.Context;
import android.dhartinews.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by jazib Shehraz on 11/19/2017.
 */

public class CommentAdapterClass extends RecyclerView.Adapter<CommentAdapterClass.viewHolder>{

    private Context context;
    private ArrayList<ArticleDataClass> data;

    public CommentAdapterClass(Context context, ArrayList<ArticleDataClass> data)
    {
        this.data = data;
        this.context = context;
    }
    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.item_comment,parent,false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        try {
            holder.txtComment.setText(data.get(position).getComment().getString("comment"));
            holder.txtUserName.setText(data.get(position).getComment().getString("userName"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class viewHolder extends RecyclerView.ViewHolder {

        TextView txtComment;
        TextView txtUserName;
        public viewHolder(View itemView) {
            super(itemView);

            txtComment = (TextView)itemView.findViewById(R.id.txtComment);
            txtUserName = (TextView)itemView.findViewById(R.id.txtUserName);
        }
    }
}
