package android.dhartinews.Home.ArticleFragment;

import android.content.Context;
import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.dhartinews.R;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment implements ArticleAdapterClass.getClickOnRow {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ArticleFragment() {
        // Required empty public constructor
    }


    private static  String userType = "";
    private static String userName = "";
    public static ArticleFragment newInstance(String uType, String uName) {
        ArticleFragment fragment = new ArticleFragment();

        userType = uType;
        userName = uName;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    WsManager wsManager;
    JSONObject object;
    WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private RecyclerView recyclerViewArticle;
    private LinearLayoutManager articleLayoutManger;

    private ArticleAdapterClass articleAdapterClass;
    private ArticleDataClass articleDataClass;
    private JSONObject responseObject;
    private ArrayList<ArticleDataClass> articleDataClassArrayList;
    private ImageView imgLastUpdateArticle;
    private TextView txtLastUpdatedArticle;


    private String url = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article, container, false);

        setServerAddress();

        imgLastUpdateArticle = (ImageView)view.findViewById(R.id.imgLastUpdated) ;
        txtLastUpdatedArticle = (TextView) view.findViewById(R.id.txtLastUpdatedArticle);

        wsManager = new WsManager(getActivity());
        recyclerViewArticle = (RecyclerView) view.findViewById(R.id.recyclerviewArticle);
        articleLayoutManger = new LinearLayoutManager(getActivity());
        articleDataClassArrayList = new ArrayList<>();

        object = new JSONObject();
        post();
        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        // mWaveSwipeRefreshLayout.setWaveColor(Color.argb(100,255,0,0));
        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                articleDataClassArrayList.clear();
                post();

            }
        });

        return view;
    }


    private void setServerAddress()
    {
        if(userType.equals("admin"))
        {
            url = Url.SERVER_URL + "/dn/view/admin_view_article.php";
        }

        if(userType.equals("user"))
        {
            url =  Url.SERVER_URL + "/dn/view/view_article%20.php" ;
        }
    }


    private void populateLastNews() {
        try {
            if (responseObject != null) {
                JSONArray array = responseObject.getJSONArray("lastRow");

                JSONObject lastArticle = array.getJSONObject(0);

                String idd = lastArticle.getString("id");
                String serverPath = Url.SERVER_URL + "/dn/image/art/" + idd + ".png";
                WsManager.loadImage(getActivity(), serverPath, imgLastUpdateArticle);

                txtLastUpdatedArticle.setText(lastArticle.getString("title"));
                articleDataClass = new ArticleDataClass();
                articleDataClass.setArticleLastObject(lastArticle);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateArticleList() {
        try {
            articleDataClassArrayList.clear();
            if (responseObject != null) { // for safety check
                JSONArray array = responseObject.getJSONArray("articles");
                for (int i = 0; i < array.length(); i++) {

                    JSONObject news = array.getJSONObject(i);

                    articleDataClass = new ArticleDataClass(); // instant of getter setter class
                    articleDataClass.setArticleObject(news);  // set news object in news setter
                    articleDataClassArrayList.add(articleDataClass);

                }
            }
            articleAdapterClass = new ArticleAdapterClass(getActivity(), articleDataClassArrayList);
            articleAdapterClass.setClickOnRow(this);
            recyclerViewArticle.setLayoutManager(articleLayoutManger);
            recyclerViewArticle.setAdapter(articleAdapterClass);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void post() {

        wsManager.post(object, url, new WSResponse() {

            @Override
            public void onSuccess(JSONObject data) {

                mWaveSwipeRefreshLayout.setRefreshing(false);
                responseObject = data;
                populateArticleList();
                populateLastNews();
                Log.d("article", data.toString());

            }

            @Override
            public void onError(String error) {
                super.onError(error);
            }
        });
    }



    @Override
    public void getClickedArticle(View view, ArrayList<ArticleDataClass> data, int position) {

        try {
            String id = data.get(position).getArticleObject().getString("id");
            String imageUrl = data.get(position).getArticleObject().getString("image");
            String title = data.get(position).getArticleObject().getString("title");
            String description = data.get(position).getArticleObject().getString("description");
            String serverPath = Url.SERVER_URL + "/dn/image/art/"+ id + ".png";

            Intent intent = new Intent(getActivity(), ArticleDetailActivity.class);
            intent.putExtra("id",id);
            intent.putExtra("image", serverPath);
            intent.putExtra("title", title);
            intent.putExtra("USER_TYPE",userType);
            intent.putExtra("USER_NAME",userName);
            intent.putExtra("description", description);

            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(getActivity(), position + " ", Toast.LENGTH_SHORT).show();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    public void onResume(){
        super.onResume();
        post();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
