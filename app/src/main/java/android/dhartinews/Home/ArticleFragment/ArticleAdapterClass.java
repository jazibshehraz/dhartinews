package android.dhartinews.Home.ArticleFragment;

import android.content.Context;
import android.dhartinews.R;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by jazib Shehraz on 11/8/2017.
 */

public class ArticleAdapterClass extends RecyclerView.Adapter<ArticleAdapterClass.viewHolder> {


    private Context context;
    private ArrayList<ArticleDataClass> data;

    public getClickOnRow getClickOnRow;
    public void setClickOnRow(getClickOnRow getClickedOnRow)
    {
        this.getClickOnRow = getClickedOnRow;

    }

    public ArticleAdapterClass(Context context, ArrayList<ArticleDataClass> data)
    {
        this.context = context;
        this.data = data;
    }
    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_article,parent,false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        try {
            holder.txtTitle.setText(data.get(position).getArticleObject().getString("title"));
            holder.txtSource.setText(data.get(position).getArticleObject().getString("source"));

            String id = data.get(position).getArticleObject().getString("id");
            String serverPath = Url.SERVER_URL + "/dn/image/art/"+ id + ".png";
            WsManager.loadImage(context, serverPath, holder.imgNews);
           // WsManager.loadImage(context, data.get(position).getArticleObject().getString("image"),holder.imgNews);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtTitle;
        TextView txtSource;
        ImageView imgNews;
        public viewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtSource = (TextView)itemView.findViewById(R.id.txtSource);
            imgNews = (ImageView)itemView.findViewById(R.id.imgNews);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            getClickOnRow.getClickedArticle(view, data, position);
        }
    }

    public interface getClickOnRow
    {
        public void getClickedArticle(View view, ArrayList<ArticleDataClass> data, int position);
    }
}
