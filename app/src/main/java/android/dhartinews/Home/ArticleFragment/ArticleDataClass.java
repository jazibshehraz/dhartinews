package android.dhartinews.Home.ArticleFragment;

import org.json.JSONObject;

/**
 * Created by jazib Shehraz on 11/8/2017.
 */

public class ArticleDataClass {

    public JSONObject getArticleObject() {
        return articleObject;
    }

    public void setArticleObject(JSONObject articleObject) {
        this.articleObject = articleObject;
    }

    private JSONObject articleObject;

    public JSONObject getArticleLastObject() {
        return articleLastObject;
    }

    public void setArticleLastObject(JSONObject articleLastObject) {
        this.articleLastObject = articleLastObject;
    }

    private JSONObject articleLastObject;

    public JSONObject getComment() {
        return comment;
    }

    public void setComment(JSONObject comment) {
        this.comment = comment;
    }

    private JSONObject comment;
}
