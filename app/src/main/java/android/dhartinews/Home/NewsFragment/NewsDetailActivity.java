package android.dhartinews.Home.NewsFragment;

import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

import static android.dhartinews.R.id.toolbar;

public class NewsDetailActivity extends AppCompatActivity {

    private TextView txtDetailNewsTitle;
    private TextView txtDetailNewsDescription;
    private ImageView imgDetailNews;
    private Button btnApprovedNews;

    private String id = "";
    private String imageUrl = "";
    private String title = "";
    private String description = "";
    private String userType = "";
    private WsManager wsManager;
    private String url = Url.SERVER_URL + "/dn/update/update_news.php";
    private HashMap<String, String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        //mplemeting back button on top of tab
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });

        wsManager = new WsManager(this);
        data = new HashMap<>();

        txtDetailNewsTitle = (TextView) findViewById(R.id.txtDetailsNewsTitle);
        txtDetailNewsDescription = (TextView) findViewById(R.id.txtDetailsNewsDescription);
        imgDetailNews = (ImageView) findViewById(R.id.imgDetailNews);
        btnApprovedNews = (Button) findViewById(R.id.btnApprovedNews);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        imageUrl = intent.getStringExtra("image");
        title = intent.getStringExtra("title");
        description = intent.getStringExtra("description");
        userType = intent.getStringExtra("USER_TYPE");
        populateNews();

        data.put("id", id);
        //data.put("status", "true");

        if (userType.equals("admin")) {
            btnApprovedNews.setVisibility(View.VISIBLE);
            btnApprovedNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();

                    wsManager.postAsMap(data, url, new WSResponse() {
                        @Override
                        public void onError(String error) {

                            super.onError(error);
                           // Log.d("error",error.toString());
                            Toast.makeText(NewsDetailActivity.this,"Problem in approving news",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                             Log.d("response",data.toString());

                            Toast.makeText(NewsDetailActivity.this,"Article Approved Successfully",Toast.LENGTH_LONG).show();
                            finish();

                        }
                    });
                }
            });
        }
    }

    private void populateNews() {
        WsManager.loadImage(this, imageUrl, imgDetailNews);
        txtDetailNewsTitle.setText(title);
        txtDetailNewsDescription.setText(description);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
