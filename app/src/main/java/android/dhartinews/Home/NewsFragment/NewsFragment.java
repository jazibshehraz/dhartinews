package android.dhartinews.Home.NewsFragment;

import android.content.Context;
import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.dhartinews.R;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragment extends Fragment implements NewsAdapterClass.getClickOnRow {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NewsFragment() {
        // Required empty public constructor
    }


    private static  String userType = "";

    public static NewsFragment newInstance(String uType, String param2) {
        NewsFragment fragment = new NewsFragment();

        userType = uType;
        return fragment;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    WsManager wsManager;
    JSONObject object;
    WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private RecyclerView recyclerViewNews;
    private LinearLayoutManager newsLayoutManger;
    private NewsAdapterClass newsAdapter;
    private NewsDataClass newsDataClass;
    private JSONObject responseObject;
    private ArrayList<NewsDataClass> newsDataClassArrayList;
    private ImageView imgLastUpdated;
    private TextView txtLastUpdatedNews;

   // private String url = "http://192.168.15.90:8086/dn/view/view_news.php";
    private String url = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);


        setServerAddress(); // set server addresss...

        imgLastUpdated = (ImageView) view.findViewById(R.id.imgLastUpdated);
        txtLastUpdatedNews = (TextView) view.findViewById(R.id.txtLastUpdatedAdd);

        wsManager = new WsManager(getActivity());
        recyclerViewNews = (RecyclerView) view.findViewById(R.id.recyclerview);
        newsLayoutManger = new LinearLayoutManager(getActivity());
        newsDataClassArrayList = new ArrayList<>();

      //  object = new JSONObject();
        post();
        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        // mWaveSwipeRefreshLayout.setWaveColor(Color.argb(100,255,0,0));
        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                newsDataClassArrayList.clear();
                post();

            }
        });
        return view;
    }

    private void showImage(String encodedImage)
    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imgLastUpdated.setImageBitmap(decodedByte);
    }
    private void setServerAddress()
    {
        if(userType.equals("admin"))
        {
            url = Url.SERVER_URL + "/dn/view/admin_view_news.php";
        }

        if(userType.equals("user"))
        {
          url =  Url.SERVER_URL + "/dn/view/view_news.php" ;
        }
    }


    private void populateLastNews() {
        try {
            if (responseObject != null) {
                JSONArray array = responseObject.getJSONArray("lastRow");


                JSONObject lastNews = array.getJSONObject(0);
                String idd = lastNews.getString("id");
                String serverPath = Url.SERVER_URL + "/dn/image/new_img/" + idd + ".png";
               WsManager.loadImage(getActivity(), serverPath, imgLastUpdated);
                txtLastUpdatedNews.setText(lastNews.getString("title"));
                newsDataClass = new NewsDataClass();
                newsDataClass.setLastNewsObject(lastNews);
               // showImage(lastNews.getString("image"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateNewsList() {
        try {
            newsDataClassArrayList.clear();
            if (responseObject != null) { // for safety check
                JSONArray array = responseObject.getJSONArray("news");
                for (int i = 0; i < array.length(); i++) {

                    JSONObject news = array.getJSONObject(i);

                    newsDataClass = new NewsDataClass(); // instant of getter setter class
                    newsDataClass.setNewsObject(news);  // set news object in news setter
                    newsDataClassArrayList.add(newsDataClass);

                }
            }
            newsAdapter = new NewsAdapterClass(getActivity(), newsDataClassArrayList);
            newsAdapter.setClickOnRow(this);
            recyclerViewNews.setLayoutManager(newsLayoutManger);
            recyclerViewNews.setAdapter(newsAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void post() {

        wsManager.post(object, url, new WSResponse() {

            @Override
            public void onSuccess(JSONObject data) {

                mWaveSwipeRefreshLayout.setRefreshing(false);
                responseObject = data;
                populateNewsList();
                populateLastNews();
                Log.d("Response", data.toString());

            }

            @Override
            public void onError(String error) {
                super.onError(error);
            }
        });
    }

    @Override
    public void getClickedNews(View view, ArrayList<NewsDataClass> data, int position) {

        try {
            String id = data.get(position).getNewsObject().getString("id");
            String imageUrl = data.get(position).getNewsObject().getString("image");
            String title = data.get(position).getNewsObject().getString("title");
            String description = data.get(position).getNewsObject().getString("description");
            String serverPath = Url.SERVER_URL + "/dn/image/new_img/" + id + ".png";

            Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
            intent.putExtra("id",id);
            intent.putExtra("image", serverPath);
            intent.putExtra("title", title);
            intent.putExtra("USER_TYPE",userType);
            intent.putExtra("description", description);

            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
      //  Toast.makeText(getActivity(), position + " ", Toast.LENGTH_SHORT).show();

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onResume(){
        super.onResume();
        post();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
