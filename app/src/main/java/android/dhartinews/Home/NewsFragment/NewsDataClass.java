package android.dhartinews.Home.NewsFragment;

import org.json.JSONObject;

/**
 * Created by jazib Shehraz on 11/4/2017.
 */

public class NewsDataClass {

    public JSONObject getNewsObject() {
        return newsObject;
    }

    public void setNewsObject(JSONObject newsObject) {
        this.newsObject = newsObject;
    }

    private JSONObject newsObject;

    public JSONObject getLastNewsObject() {
        return lastNewsObject;
    }

    public void setLastNewsObject(JSONObject lastNewsObject) {
        this.lastNewsObject = lastNewsObject;
    }

    private JSONObject lastNewsObject;
}
