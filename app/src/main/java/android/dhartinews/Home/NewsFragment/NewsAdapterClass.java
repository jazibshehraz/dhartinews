package android.dhartinews.Home.NewsFragment;

import android.content.Context;
import android.dhartinews.R;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by jazib Shehraz on 11/4/2017.
 */

public class NewsAdapterClass extends RecyclerView.Adapter<NewsAdapterClass.viewHolder> {

    private Context context;
    private ArrayList<NewsDataClass> data;

    public getClickOnRow getClickOnRow;

    public void setClickOnRow(getClickOnRow getClickedOnRow) {
        this.getClickOnRow = getClickedOnRow;

    }

    public NewsAdapterClass(Context context, ArrayList<NewsDataClass> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        try {
            holder.txtTitle.setText(data.get(position).getNewsObject().getString("title"));
            holder.txtSource.setText(data.get(position).getNewsObject().getString("source"));

            String id = data.get(position).getNewsObject().getString("id");
            String serverPath = Url.SERVER_URL + "/dn/image/new_img/" + id + ".png";
            WsManager.loadImage(context, serverPath, holder.imgNews);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtTitle;
        TextView txtSource;
        ImageView imgNews;

        public viewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSource = (TextView) itemView.findViewById(R.id.txtSource);
            imgNews = (ImageView) itemView.findViewById(R.id.imgNews);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            getClickOnRow.getClickedNews(view, data, position);
        }
    }

    public interface getClickOnRow {
        public void getClickedNews(View view, ArrayList<NewsDataClass> data, int position);
    }
}
