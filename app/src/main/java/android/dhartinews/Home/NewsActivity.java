package android.dhartinews.Home;

import android.content.Intent;
import android.dhartinews.Home.NewsFragment.NewsAdapterClass;
import android.dhartinews.Home.NewsFragment.NewsDataClass;
import android.dhartinews.Home.NewsFragment.NewsDetailActivity;
import android.dhartinews.R;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

public class NewsActivity extends AppCompatActivity implements NewsAdapterClass.getClickOnRow {

    WsManager wsManager;
    JSONObject object;
    WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private RecyclerView recyclerViewNews;
    private LinearLayoutManager newsLayoutManger;
    private NewsAdapterClass newsAdapter;
    private NewsDataClass newsDataClass;
    private JSONObject responseObject;
    private ArrayList<NewsDataClass> newsDataClassArrayList;
    private ImageView imgLastUpdated;
    private TextView txtLastUpdatedNews;

    //private String url = "http://192.168.100.3:8086/dn/index.php";
    private String url = "http://10.0.11.197:8086/dn/index.php";

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);


        imgLastUpdated = (ImageView) findViewById(R.id.imgLastUpdated);
        txtLastUpdatedNews = (TextView) findViewById(R.id.txtLastUpdatedAdd);

        wsManager = new WsManager(this);
        recyclerViewNews = (RecyclerView) findViewById(R.id.recyclerview);
        newsLayoutManger = new LinearLayoutManager(this);
        newsDataClassArrayList = new ArrayList<>();

        object = new JSONObject();
        post();
        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        // mWaveSwipeRefreshLayout.setWaveColor(Color.argb(100,255,0,0));
        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                newsDataClassArrayList.clear();
                post();

            }
        });
    }

    private void populateLastNews() {
        try {
            if (responseObject != null) {
                JSONArray array = responseObject.getJSONArray("lastRow");

                JSONObject lastNews = array.getJSONObject(0);

                WsManager.loadImage(this, lastNews.getString("image"), imgLastUpdated);
                txtLastUpdatedNews.setText(lastNews.getString("title"));
                newsDataClass = new NewsDataClass();
                newsDataClass.setLastNewsObject(lastNews);


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateNewsList() {
        try {
            if (responseObject != null) { // for safety check
                JSONArray array = responseObject.getJSONArray("news");
                for (int i = 0; i < array.length(); i++) {

                    JSONObject news = array.getJSONObject(i);

                    newsDataClass = new NewsDataClass(); // instant of getter setter class
                    newsDataClass.setNewsObject(news);  // set news object in news setter
                    newsDataClassArrayList.add(newsDataClass);

                }
            }
            newsAdapter = new NewsAdapterClass(this, newsDataClassArrayList);
            newsAdapter.setClickOnRow(this);
            recyclerViewNews.setLayoutManager(newsLayoutManger);
            recyclerViewNews.setAdapter(newsAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void post() {

        wsManager.post(object, url, new WSResponse() {

            @Override
            public void onSuccess(JSONObject data) {

                mWaveSwipeRefreshLayout.setRefreshing(false);
                responseObject = data;
                populateNewsList();
                populateLastNews();
                Log.d("Response", data.toString());

            }

            @Override
            public void onError(String error) {
                super.onError(error);
            }
        });
    }

    @Override
    public void getClickedNews(View view, ArrayList<NewsDataClass> data, int position) {

        try {
            String imageUrl = data.get(position).getNewsObject().getString("image");
            String title = data.get(position).getNewsObject().getString("title");
            String description = data.get(position).getNewsObject().getString("description");

            Intent intent = new Intent(this,NewsDetailActivity.class);
            intent.putExtra("image",imageUrl);
            intent.putExtra("title",title);
            intent.putExtra("description",description);

            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }





        Toast.makeText(getApplicationContext(), position + " ", Toast.LENGTH_SHORT).show();

    }

}
