package android.dhartinews.Home.AdvertisementsFragment;

import android.content.Context;
import android.database.Cursor;
import android.dhartinews.R;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by jazib Shehraz on 11/9/2017.
 */

public class AdvertisementAdapterClass extends RecyclerView.Adapter<AdvertisementAdapterClass.viewHolder> {


    private Context context;
    private ArrayList<AdvertisementDataClass> data;
    private Bitmap bitmap;

    public getClickOnRow getClickOnRow;

    public void setClickOnRow(getClickOnRow getClickedOnRow) {
        this.getClickOnRow = getClickedOnRow;

    }

    public AdvertisementAdapterClass(Context context, ArrayList<AdvertisementDataClass> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_add, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    String uri = "";
    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        try {
            holder.txtTitle.setText(data.get(position).getAdvertisementObect().getString("title"));
            holder.txtSource.setText(data.get(position).getAdvertisementObect().getString("source"));
            String id = data.get(position).getAdvertisementObect().getString("id");
            String serverPath = Url.SERVER_URL + "/dn/image/add/"+ id + ".png";
            WsManager.loadImage(context, serverPath, holder.imgNews);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtTitle;
        TextView txtSource;
        ImageView imgNews;

        public viewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSource = (TextView) itemView.findViewById(R.id.txtSource);
            imgNews = (ImageView) itemView.findViewById(R.id.imgAdd);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            getClickOnRow.getClickedNews(view, data, position);
        }
    }

    public interface getClickOnRow {
        public void getClickedNews(View view, ArrayList<AdvertisementDataClass> data, int position);
    }
}
