package android.dhartinews.Home.AdvertisementsFragment;

import org.json.JSONObject;

/**
 * Created by jazib Shehraz on 11/9/2017.
 */

public class AdvertisementDataClass {


    public JSONObject getAdvertisementObect() {
        return advertisementObect;
    }

    public void setAdvertisementObect(JSONObject advertisementObect) {
        this.advertisementObect = advertisementObect;
    }

    public JSONObject getLastAdvertisementOjbect() {
        return lastAdvertisementOjbect;
    }

    public void setLastAdvertisementOjbect(JSONObject lastAdvertisementOjbect) {
        this.lastAdvertisementOjbect = lastAdvertisementOjbect;
    }

    private JSONObject advertisementObect;
    private JSONObject lastAdvertisementOjbect;
}
