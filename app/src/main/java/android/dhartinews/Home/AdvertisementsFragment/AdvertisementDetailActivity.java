package android.dhartinews.Home.AdvertisementsFragment;

import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;

public class AdvertisementDetailActivity extends AppCompatActivity {

    private TextView txtDetailTitle;
    private TextView txtDetailDescription;
    private ImageView imgDetail;

    private String id = "";
    private String imageUrl = "";
    private String title = "";
    private String description = "";
    private String userType = "";
    private String url = Url.SERVER_URL + "/dn/update/update_advertisement.php";
    private HashMap<String, String> data;
    private WsManager wsManager;
    private Button btnApprovedArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // toolbar back button

        txtDetailTitle = (TextView) findViewById(R.id.txtDetailsTitle);
        txtDetailDescription = (TextView) findViewById(R.id.txtDetailsDescription);
        imgDetail = (ImageView) findViewById(R.id.imgDetail);
        btnApprovedArticle = (Button) findViewById(R.id.btnApproved);
        wsManager = new WsManager(this);
        data = new HashMap<>();

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        imageUrl = intent.getStringExtra("image");
        title = intent.getStringExtra("title");
        description = intent.getStringExtra("description");
        userType = intent.getStringExtra("USER_TYPE");

        populateAdd();


        data.put("id", id);
        //data.put("status", "true");

        if (userType.equals("admin")) {

            btnApprovedArticle.setVisibility(View.VISIBLE);
            btnApprovedArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();

                    wsManager.postAsMap(data, url, new WSResponse() {
                        @Override
                        public void onError(String error) {

                            super.onError(error);
                            Log.d("error", error.toString());
                            Toast.makeText(AdvertisementDetailActivity.this,"Problem in approving add",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            Toast.makeText(AdvertisementDetailActivity.this,"Article Approved Successfully",Toast.LENGTH_LONG).show();
                            Log.d("response", data.toString());
                            finish();
                        }
                    });
                }
            });
        }
    }

    private void populateAdd() {

        WsManager.loadImage(this, imageUrl, imgDetail);
        txtDetailTitle.setText(title);
        txtDetailDescription.setText(description);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
