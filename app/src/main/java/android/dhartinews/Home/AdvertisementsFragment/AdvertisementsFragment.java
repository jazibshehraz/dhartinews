package android.dhartinews.Home.AdvertisementsFragment;

import android.content.Context;
import android.content.Intent;
import android.dhartinews.Home.NewsFragment.NewsDetailActivity;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.dhartinews.R;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdvertisementsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdvertisementsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdvertisementsFragment extends Fragment implements AdvertisementAdapterClass.getClickOnRow {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AdvertisementsFragment() {
        // Required empty public constructor
    }

    private static  String userType = "";
    public static AdvertisementsFragment newInstance(String uType, String param2) {
        AdvertisementsFragment fragment = new AdvertisementsFragment();
       userType = uType;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    WsManager wsManager;
    JSONObject object;
    WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager LayoutManger;
    private AdvertisementAdapterClass advertisementAdapterClass;
    private AdvertisementDataClass advertisementDataClass;
    private JSONObject responseObject;
    private ArrayList<AdvertisementDataClass> advertisementDataClassArrayList;
    private TextView txtLastUpdatedAdd;
    private ImageView imgLastUpdatedAdd;

    private String url = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_advertisements, container, false);

        setServerAddress();

        imgLastUpdatedAdd = (ImageView)view.findViewById(R.id.imgLastUpdated);
        txtLastUpdatedAdd = (TextView) view.findViewById(R.id.txtLastUpdatedAdd);

        wsManager = new WsManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        LayoutManger = new LinearLayoutManager(getActivity());
        advertisementDataClassArrayList = new ArrayList<>();

        object = new JSONObject();
        post();
        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        // mWaveSwipeRefreshLayout.setWaveColor(Color.argb(100,255,0,0));
        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                advertisementDataClassArrayList.clear();
                post();

            }
        });

        return view;
    }

    private void setServerAddress()
    {
        if(userType.equals("admin"))
        {
            url = Url.SERVER_URL + "/dn/view/admin_view_advertisement.php";
        }

        if(userType.equals("user"))
        {
            url =  Url.SERVER_URL + "/dn/view/view_advertisement%20.php" ;
        }
    }

    private void populateLastAdd() {
        try {
            if (responseObject != null) {
                JSONArray array = responseObject.getJSONArray("lastRow");

                JSONObject lastNews = array.getJSONObject(0);

                txtLastUpdatedAdd.setText(lastNews.getString("title"));

                String idd = lastNews.getString("id");
                String serverPath = Url.SERVER_URL + "/dn/image/add/" + idd + ".png";
                WsManager.loadImage(getActivity(), serverPath, imgLastUpdatedAdd);

                advertisementDataClass = new AdvertisementDataClass();
                advertisementDataClass.setLastAdvertisementOjbect(lastNews);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populateAddList() {
        try {
            advertisementDataClassArrayList.clear();
            if (responseObject != null) { // for safety check
                JSONArray array = responseObject.getJSONArray("advertisements");
                for (int i = 0; i < array.length(); i++) {

                    JSONObject news = array.getJSONObject(i);

                    advertisementDataClass = new AdvertisementDataClass(); // instant of getter setter class
                    advertisementDataClass.setAdvertisementObect(news);  // set news object in news setter
                    advertisementDataClassArrayList.add(advertisementDataClass);

                }
            }
            advertisementAdapterClass = new AdvertisementAdapterClass(getActivity(), advertisementDataClassArrayList);
            advertisementAdapterClass.setClickOnRow(this);
            recyclerView.setLayoutManager(LayoutManger);
            recyclerView.setAdapter(advertisementAdapterClass);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void post() {

        wsManager.post(object, url, new WSResponse() {

            @Override
            public void onSuccess(JSONObject data) {

                mWaveSwipeRefreshLayout.setRefreshing(false);
                responseObject = data;
                populateAddList();
                populateLastAdd();
                Log.d("Response", data.toString());

            }

            @Override
            public void onError(String error) {
                super.onError(error);
            }
        });
    }

    @Override
    public void getClickedNews(View view, ArrayList<AdvertisementDataClass> data, int position) {

        try {
            String id = data.get(position).getAdvertisementObect().getString("id");
            String imageUrl = data.get(position).getAdvertisementObect().getString("image");
            String title = data.get(position).getAdvertisementObect().getString("title");
            String description = data.get(position).getAdvertisementObect().getString("description");
            String serverPath = Url.SERVER_URL + "/dn/image/add/"+ id + ".png";

            Intent intent = new Intent(getActivity(), AdvertisementDetailActivity.class);
            intent.putExtra("id",id);
            intent.putExtra("image", serverPath);
            intent.putExtra("title", title);
            intent.putExtra("USER_TYPE",userType);
            intent.putExtra("description", description);

            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onResume(){
        super.onResume();
        post();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
