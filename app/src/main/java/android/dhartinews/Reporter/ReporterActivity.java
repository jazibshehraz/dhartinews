package android.dhartinews.Reporter;


import android.app.ProgressDialog;
import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ReporterActivity extends AppCompatActivity {

    /*

    Tutorial URL ::  https://androidjson.com/android-upload-image-server-using-php-mysql/
     */

    Bitmap bitmap;
    Button btnSelectImage, btnUploadImage;
    ImageView imageView;
    TextInputLayout editTitle;
    TextInputLayout editDescription;
    private WsManager wsManager;
    private HashMap<String, String> data;
    private String image = "";
    private String sourceName = "dummy source";
    boolean valid=true;
    private String url = Url.SERVER_URL + "/dn/insert/insert_news.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wsManager = new WsManager(this);
        data = new HashMap<String, String>();

        imageView = (ImageView) findViewById(R.id.imgSelectedNews);
        btnSelectImage = (Button) findViewById(R.id.btnSelectImage);
        btnUploadImage = (Button) findViewById(R.id.btnUploadArticle);
        editTitle = ( TextInputLayout)findViewById(R.id.inputNewsTitle);
        editDescription = (TextInputLayout)findViewById(R.id.inputNewsDescription);

        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);

            }
        });

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
                if(valid==true){
                    try {
                        convetImageToString();
                        data.put("title", editTitle.getEditText().getText().toString());
                        data.put("description", editDescription.getEditText().getText().toString());
                        data.put("source", sourceName);
                        data.put("timestamp", getTodayDateString());
                        data.put("image", image);

                        Log.d("data is ", data.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    wsManager.postAsMap(data, url, new WSResponse() {
                        @Override
                        public void onError(String error) {
                            super.onError(error);

                            Log.d("error is ",error);
                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            try {

                                if(data.getString("status").equals("true"))
                                {
                                    editDescription.getEditText().setText("");
                                    editTitle.getEditText().setText("");
                                    Toast.makeText(getApplicationContext(),"News Upload Successfully",Toast.LENGTH_LONG).show();
                                }
                                Log.d("response",data.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else{
                    valid=false;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {

            Uri uri = I.getData();

            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }


    private Date today() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }


    private String getTodayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(today());
    }

    private void convetImageToString() {
        ByteArrayOutputStream byteArrayOutputStreamObject;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        image = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

    }

    private void convertImageFromString() {
        byte[] decodedString = Base64.decode("/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAQEBAQE", Base64.DEFAULT);

        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
    private void validation(){
        if(editTitle.getEditText().getText().toString().trim().isEmpty()){
            editTitle.setError("plz enter title");
            valid=false;
        }
        if(editDescription.getEditText().getText().toString().trim().isEmpty()){
            editDescription.setError("plz enter description");
            valid=false;
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}


//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.dhartinews.Test.TestActivity;
//import android.dhartinews.Variable.Url;
//import android.dhartinews.WsManager.WSResponse;
//import android.dhartinews.WsManager.WsManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.provider.MediaStore;
//import android.support.design.widget.TextInputLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.dhartinews.R;
//import android.util.Base64;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//
//public class ReporterActivity extends AppCompatActivity {
//
//    /*
//
//    Tutorial URL ::  https://androidjson.com/android-upload-image-server-using-php-mysql/
//     */
//
//    Bitmap bitmap;
//    Button btnSelectImage, btnUploadImage;
//    ImageView imageView;
//    TextInputLayout editTitle;
//    TextInputLayout editDescription;
//    private WsManager wsManager;
//    private HashMap<String, String> data;
//    private String image = "";
//    private String sourceName = "dummy source";
//    private String url = Url.SERVER_URL + "/dn/insert/insert_news.php";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_reporter);
//
//        wsManager = new WsManager(this);
//        data = new HashMap<String, String>();
//
//        imageView = (ImageView) findViewById(R.id.imgSelectedNews);
//        btnSelectImage = (Button) findViewById(R.id.btnSelectImage);
//        btnUploadImage = (Button) findViewById(R.id.btnUploadArticle);
//        editTitle = ( TextInputLayout)findViewById(R.id.inputNewsTitle);
//        editDescription = (TextInputLayout)findViewById(R.id.inputNewsDescription);
//
//        btnSelectImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);
//
//            }
//        });
//
//        btnUploadImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
//                convetImageToString();
//
//                    data.put("title", editTitle.getEditText().getText().toString());
//                    data.put("description", editDescription.getEditText().getText().toString());
//                    data.put("source", sourceName);
//                    data.put("timestamp", getTodayDateString());
//                    data.put("image", image);
//
//                    Log.d("test data is ", data.toString());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                wsManager.postAsMap(data, url, new WSResponse() {
//                    @Override
//                    public void onError(String error) {
//                        super.onError(error);
//                    }
//
//                    @Override
//                    public void onSuccess(JSONObject data) {
//
//                        // Log.d("response",data.toString());
//                    }
//                });
//            }
//        });
//    }
//
//    @Override
//    protected void onActivityResult(int RC, int RQC, Intent I) {
//
//        super.onActivityResult(RC, RQC, I);
//
//        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {
//
//            Uri uri = I.getData();
//
//            try {
//
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//                imageView.setImageBitmap(bitmap);
//
//            } catch (IOException e) {
//
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    private Date today() {
//        final Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, 0);
//        return cal.getTime();
//    }
//
//
//    private String getTodayDateString() {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormat.format(today());
//    }
//
//    private void convetImageToString() {
//        ByteArrayOutputStream byteArrayOutputStreamObject;
//
//        byteArrayOutputStreamObject = new ByteArrayOutputStream();
//
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
//
//        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
//
//        image = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
//        Log.d("converted image",image.toString());
//
//    }
//
//    private void convertImageFromString() {
//        byte[] decodedString = Base64.decode("/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
//                "AQEBAQEBAQEBAQEBAQEBAQE", Base64.DEFAULT);
//
//        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//    }
//
//}
