package android.dhartinews.Admin;

import android.content.Context;
import android.dhartinews.Login.Classes.UserDataClass;
import android.dhartinews.Login.Classes.Message;
import android.dhartinews.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mirza Shafique on 13/11/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    ArrayList<UserDataClass> data;
    Context context;

    public UserAdapter(Context context, ArrayList<UserDataClass> list) {
        this.context = context;
        this.data=list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public UserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_user_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserAdapter.MyViewHolder holder, int position) {
        try {
            holder.uname.setText(data.get(position).getData().getString("username"));
            holder.utype.setText(data.get(position).getData().getString("usertype"));
            holder.ustatus.setText(data.get(position).getData().getString("status"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // holder.txtSource.setText(data.get(position).getNewsObject().getString("source"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView uname,ustatus,utype;

        public MyViewHolder(View itemView) {
            super(itemView);
            uname = (TextView) itemView.findViewById(R.id.txtUserName);
            utype=(TextView)itemView.findViewById(R.id.txtUserType);
            ustatus=(TextView)itemView.findViewById(R.id.txtUserStatus);
        }
    }

//    public void addItem(String name,int hours, int mints){
//
//        userDataClass current = new userDataClass();
//        current.names = name;
//        current.hours=hours;
//        current.mints=mints;
//        //mListData.add(item);
//        data.add(current);
//        notifyItemInserted(data.size());
//    }
//    public void removeItem(String name){
//        int position=data.indexOf(name);
//        if(position != -1){
//            data.remove(position);
//            Message.message(context,""+position);
//            notifyItemRemoved(position);
//        }
//    }
    public void removeItem(int position){
        data.remove(position);
        Message.message(context,""+position);
        notifyItemRemoved(position);
    }
//    public void updateRow(int position){
//
//        data.set(position,)
//        Message.message(context,""+position);
//        notifyItemChanged(position);
//    }
}