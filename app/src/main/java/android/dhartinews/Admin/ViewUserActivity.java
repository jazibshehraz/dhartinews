package android.dhartinews.Admin;

import android.app.FragmentTransaction;
import android.content.Context;
import android.dhartinews.Login.Classes.UserDataClass;
import android.dhartinews.Login.Fragments.UpdateUserFragment;
import android.dhartinews.R;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewUserActivity extends AppCompatActivity implements UpdateUserFragment.UpdateCommunicator {
    TextView textView;
    RecyclerView recyclerViewUser;
    UserAdapter userAdapter;
    WsManager wsManager;
    JSONObject object;
    JSONObject responseObject;
    UserDataClass userDataClass;
    ArrayList<UserDataClass> userDataClassArrayList;
    LinearLayoutManager layoutManger;
    private HashMap<String, String> data;
    private HashMap<String, String> data2;

    private String adminViewUserUrl = Url.SERVER_URL + "/dn/view/admin_view_user.php";
    private String deleteUserUrl = Url.SERVER_URL + "/dn/block_delete/admin_delete_user.php";
    private String updateUserUrl = Url.SERVER_URL + "/dn/update/admin_update_user_data.php";
    // "http://192.168.10.5/dherti_news/admin_update_user.php";
    private String updateUserStatusUrl = Url.SERVER_URL + "/dn/update/admin_update_user_status.php";
    //"http://192.168.10.5/dherti_news/admin_update_status.php";
    private String blockUserUrl = Url.SERVER_URL + "/dn/block_delete/admin_block_user.php";
    //"http://192.168.10.5/dherti_news/admin_block_user.php";
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        data = new HashMap<String, String>();
        data2 = new HashMap<>();
        userDataClassArrayList = new ArrayList<>();
        wsManager = new WsManager(this);
        layoutManger = new LinearLayoutManager(getApplicationContext());
        object = new JSONObject();
        post();  //geting data from json array userDataClassArrayList
        recyclerViewUser = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerViewUser.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerViewUser, new ClickListener() {
            @Override
            public void onClick(View view, int position) {          //interface methon for simple click

            }

            @Override
            public void onLongClick(View view, final int position) {     //interface method for long click

                final PopupMenu popupMenu = new PopupMenu(getApplicationContext(), recyclerViewUser.getChildAt(position));
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String id = null;
                        try {
                            id = userDataClassArrayList.get(position).getData().getString("id");    //to get user id of selected row
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        switch (menuItem.getItemId()) {
                            case R.id.delete:                               // delete menu start
                                try {
                                    Log.d("id is ", id);
                                    data.put("id", id);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                wsManager.postAsMap(data, deleteUserUrl, new WSResponse() {

                                    @Override
                                    public void onSuccess(JSONObject data) {

                                        try {
                                            Log.d("data is ", data.toString());
                                            if (data.getString("status").equals("true")) {
                                                userAdapter.removeItem(position);
                                                Toast.makeText(getApplicationContext(), "row is deleted from data base", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(getApplicationContext(), "problem", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override

                                    public void onError(String error) {

                                        Log.d("error ", error.toString());
                                    }
                                });

                                return true;

                            case R.id.edit:                 //update menu start
                                pos = position;
                                FragmentTransaction manager = getFragmentManager().beginTransaction();
                                //FragmentTransaction fragmentTransaction = manager.beginTransaction();
                                UpdateUserFragment nemazDialogFragment = new UpdateUserFragment();
                                nemazDialogFragment.show(manager, "My Dialog");
                                nemazDialogFragment.setCancelable(true);
                                return true;

                            case R.id.approve:              //approve menu start
                                data.put("id", id);
                                Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                                wsManager.postAsMap(data, updateUserStatusUrl, new WSResponse() {
                                    @Override
                                    public void onSuccess(JSONObject data) {
                                        if (data.optString("status") == "true") {
                                            Toast.makeText(getApplicationContext(), "status is updated from data base", Toast.LENGTH_SHORT).show();
                                            post();

                                        } else {
                                            Toast.makeText(getApplicationContext(), "problem in status updation", Toast.LENGTH_SHORT).show();
                                        }

                                        Log.d("response is", data.toString());
                                    }

                                    @Override
                                    public void onError(String error) {
                                        Log.d("error is ",error.toString());
                                        Toast.makeText(getApplicationContext(), "sir dard..!  " + error, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                return true;
                            case R.id.block:                    //block menu start
                                data.put("id", id);
                                wsManager.postAsMap(data, blockUserUrl, new WSResponse() {
                                    @Override
                                    public void onSuccess(JSONObject data) {
                                        if (data.optString("status") == "true") {
                                            Toast.makeText(getApplicationContext(), "user is blocked", Toast.LENGTH_SHORT).show();
                                            post();

                                        } else {
                                            Toast.makeText(getApplicationContext(), "problem in user block", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onError(String error) {
                                        Toast.makeText(getApplicationContext(), "sir dard..!  " + error, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                return true;
                            default:
                                return false;
                        }
//                        if(menuItem.toString()=="Delete"){
//                            Toast.makeText(getApplicationContext(),"Row is deleted",Toast.LENGTH_LONG).show();
//                            //  int c=myDataBaseAdapter.deleteRow(position);
////                            if(c>0){
////                                Toast.makeText(MainActivity.this,"Row is deleted",Toast.LENGTH_LONG).show();
////                            }
//                        }
//                        if (menuItem.toString()=="Edit"){
//                            Toast.makeText(getApplicationContext(),"edit",Toast.LENGTH_LONG).show();
//                        }
                        // return true;
                    }
                });
                popupMenu.show();

            }
        }));


    }

    private void post() {
        wsManager.post(object, adminViewUserUrl, new WSResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                Log.d("Response", data.toString());
                responseObject = data;
                populateUserList();

            }

            @Override
            public void onError(String error) {
                Log.d("Response", error.toString());
            }
        });
    }

    private void populateUserList() {
        try {
            userDataClassArrayList.clear();
            if (responseObject != null) { // for safety check
                JSONArray array = responseObject.getJSONArray("user");
                for (int i = 0; i < array.length(); i++) {

                    JSONObject user = array.getJSONObject(i);

                    userDataClass = new UserDataClass(); // instant of getter setter class
                    userDataClass.setData(user);  // set news object in news setter
                    userDataClassArrayList.add(userDataClass);

                }
            }

            userAdapter = new UserAdapter(getApplicationContext(), userDataClassArrayList);
            recyclerViewUser.setLayoutManager(layoutManger);
            recyclerViewUser.setAdapter(userAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateResponse(String userName, String email, String password, String usertype) {     // getting values of from updateFragment dialog for update user
        String id = null;
        try {
            id = userDataClassArrayList.get(pos).getData().getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        data2.put("id", id);
        data2.put("username", userName);
        data2.put("email", email);
        data2.put("password", password);
        data2.put("usertype", usertype);
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
        wsManager.postAsMap(data2, updateUserUrl, new WSResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                if (data.optString("status") == "true") {

                    Toast.makeText(getApplicationContext(), "row is updated from data base", Toast.LENGTH_SHORT).show();
                    userDataClassArrayList.clear();
                    post();

                } else {
                    Toast.makeText(getApplicationContext(), "problem in updating", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getApplicationContext(), "masla" + error, Toast.LENGTH_SHORT).show();
            }
        });

    }


    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {  //action listener for recycler view

        private ClickListener clickListener;
        private GestureDetector gestureDetector;

        RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }

                }

            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}