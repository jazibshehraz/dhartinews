package android.dhartinews.Auther;


import android.content.Intent;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class AutherActivity extends AppCompatActivity {

    Button btnUploadArticle,btnSelectImage;
    ImageView imageView;
    TextInputLayout inputArticleTitle;
    TextInputLayout inputArticleDescription;
    private HashMap<String, String> data;
    private String sourceName = "sourceName";
    private WsManager wsManager;
    boolean valid=true;
    private String url = Url.SERVER_URL + "/dn/insert/insert_article.php";
    private Bitmap bitmap;
    private String image = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auther);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.imgSelectedAdd);
        btnUploadArticle = (Button)findViewById(R.id.btnUploadArticle);
        btnSelectImage = (Button)findViewById(R.id.btnSelectImage);
        inputArticleTitle =(TextInputLayout)findViewById(R.id.inputArticleTitle);
        inputArticleDescription = (TextInputLayout)findViewById(R.id.inputArticleDescription);
        data = new HashMap<String,String>();
        wsManager = new WsManager(this);


        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);


            }
        });

        btnUploadArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                convetImageToString();
                validation();
                if(valid){
                    data.put("title",inputArticleTitle.getEditText().getText().toString());
                    data.put("description",inputArticleDescription.getEditText().getText().toString());
                    data.put("timestamp",getTodayDateString());
                    data.put("source",sourceName);
                    data.put("image",image);

                    Log.d("data is ",data.toString());

                    wsManager.postAsMap(data, url, new WSResponse() {
                        @Override
                        public void onError(String error) {

                            super.onError(error);
                            Toast.makeText(AutherActivity.this,"Article Not Inserted",Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            Log.d("response",data.toString());
                            try {
                                if(data.getString("status").equals("true"))
                                {
                                    Toast.makeText(AutherActivity.this,"Article Inseted Successfully",Toast.LENGTH_LONG).show();
                                    inputArticleTitle.getEditText().setText("");
                                    inputArticleDescription.getEditText().setText("");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else{
                    valid=false;
                }
            }
        });

    }


    Uri uri = null;

    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {

            uri = I.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                //Picasso.with(this).load(new File(String.valueOf(uri))).into(imageView);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    private void convetImageToString() {
        ByteArrayOutputStream byteArrayOutputStreamObject;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        image = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

    }


    private Date today() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }


    private String getTodayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(today());
    }
    private void validation(){
        if(inputArticleTitle.getEditText().getText().toString().trim().isEmpty()){
            inputArticleTitle.setError("plz enter title");
            valid=false;
        }
        if(inputArticleDescription.getEditText().getText().toString().trim().isEmpty()){
            inputArticleDescription.setError("plz enter description");
            valid=false;
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

//import android.dhartinews.WsManager.WSResponse;
//import android.dhartinews.WsManager.WsManager;
//import android.support.design.widget.TextInputLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.dhartinews.R;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//
//import org.json.JSONObject;
//
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//
//public class AutherActivity extends AppCompatActivity {
//
//    Button btnUploadArticle;
//    TextInputLayout inputArticleTitle;
//    TextInputLayout inputArticleDescription;
//    private HashMap<String, String> data;
//    private String sourceName = "sourceName";
//    private WsManager wsManager;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_auther);
//
//        btnUploadArticle = (Button)findViewById(R.id.btnUploadArticle);
//        inputArticleTitle =(TextInputLayout)findViewById(R.id.inputArticleTitle);
//        inputArticleDescription = (TextInputLayout)findViewById(R.id.inputArticleDescription);
//        data = new HashMap<String,String>();
//        wsManager = new WsManager(this);
//
//        btnUploadArticle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                data.put("title",inputArticleTitle.getEditText().getText().toString());
//                data.put("description",inputArticleDescription.getEditText().getText().toString());
//                data.put("timestamp",getTodayDateString());
//                data.put("source",sourceName);
//                data.put("image","image");
//
//                Log.d("data is ",data.toString());
//
//                wsManager.postAsMap(data, "http://192.168.15.90:8086/dn/insert/insert_article.php", new WSResponse() {
//                    @Override
//                    public void onError(String error) {
//                        super.onError(error);
//                    }
//
//                    @Override
//                    public void onSuccess(JSONObject data) {
//
//                        Log.d("response",data.toString());
//                    }
//                });
//            }
//        });
//
//    }
//
//
//    private Date today() {
//        final Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, 0);
//        return cal.getTime();
//    }
//
//
//    private String getTodayDateString() {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormat.format(today());
//    }
//}
