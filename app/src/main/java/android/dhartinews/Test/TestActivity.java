package android.dhartinews.Test;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.util.Base64;
import android.widget.ImageView;

public class TestActivity extends AppCompatActivity {

    ImageView img;
    String test = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);

        img = (ImageView)findViewById(R.id.img);
        Intent intent = getIntent();
        test = intent.getStringExtra("key");
        convetImage();
    }

    private void convetImage()
    {

        byte[] decodedString = Base64.decode(test, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        img.setImageBitmap(decodedByte);
    }
}
