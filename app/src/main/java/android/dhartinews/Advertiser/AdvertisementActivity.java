package android.dhartinews.Advertiser;


import android.content.Intent;
import android.database.Cursor;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.dhartinews.R;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static android.R.attr.path;

public class AdvertisementActivity extends AppCompatActivity {


    Bitmap bitmap;
    Button btnSelectImage, btnUploadImage;
    ImageView imageView;
    TextInputLayout editTitle;
    TextInputLayout editDescription;
    private WsManager wsManager;
    private HashMap<String, String> data;
    private String image = "";
    private String sourceName = "dummy source";
    boolean valid = true;
    private String url = Url.SERVER_URL + "/dn/insert/insert_advertisement.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wsManager = new WsManager(this);
        data = new HashMap<String, String>();

        imageView = (ImageView) findViewById(R.id.imgSelectedAdd);
        btnSelectImage = (Button) findViewById(R.id.btnSelectImage);
        btnUploadImage = (Button) findViewById(R.id.btnUploadAdd);
        editTitle = (TextInputLayout) findViewById(R.id.inputAddTitle);
        editDescription = (TextInputLayout) findViewById(R.id.inputAddDescription);

        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);

                //s();

            }
        });

        btnUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
                if (valid == true) {
                    try {

                        convetImageToString();
                        data.put("title", editTitle.getEditText().getText().toString());
                        data.put("description", editDescription.getEditText().getText().toString());
                        data.put("source", sourceName);
                        data.put("timestamp", getTodayDateString());
                        data.put("image", image);

                        Log.d("data is ", data.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    wsManager.postAsMap(data, url, new WSResponse() {
                        @Override
                        public void onError(String error) {
                            super.onError(error);

                            Toast.makeText(AdvertisementActivity.this, "Add Not Inserted Succcessfully", Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onSuccess(JSONObject data) {

                            Log.d("response", data.toString());
                            try {
                                if(data.getString("status").equals("true"))
                                {
                                    editTitle.getEditText().setText("");
                                    editDescription.getEditText().setText("");

                                    Toast.makeText(AdvertisementActivity.this,"Add Inserted Succcessfully",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    valid = false;
                }
            }
        });
    }

    Uri uri = null;

    @Override
    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {

            uri = I.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                //Picasso.with(this).load(new File(String.valueOf(uri))).into(imageView);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        // this is our fallback here
        return uri.getPath();
    }

    private Date today() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }


    private String getTodayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(today());
    }

    private void convetImageToString() {
        ByteArrayOutputStream byteArrayOutputStreamObject;

        byteArrayOutputStreamObject = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);

        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

        image = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

    }

    private void convertImageFromString() {
        byte[] decodedString = Base64.decode("/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
                "AQEBAQEBAQEBAQEBAQEBAQE", Base64.DEFAULT);

        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    private void validation() {

        if (editTitle.getEditText().getText().toString().trim().isEmpty()) {
            editTitle.setError("Title is missing");
            valid = false;
        }
        if (editDescription.getEditText().getText().toString().trim().isEmpty()) {
            editDescription.setError("Description is missing");
            valid = false;
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}


//
//import android.content.Intent;
//import android.dhartinews.WsManager.WSResponse;
//import android.dhartinews.WsManager.WsManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.provider.MediaStore;
//import android.support.design.widget.TextInputLayout;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.dhartinews.R;
//import android.util.Base64;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//
//public class AdvertisementActivity extends AppCompatActivity {
//
//
//    Bitmap bitmap;
//    Button btnSelectImage, btnUploadImage;
//    ImageView imageView;
//    TextInputLayout editTitle;
//    TextInputLayout editDescription;
//    private WsManager wsManager;
//    private HashMap<String, String> data;
//    private String image = "";
//    private String sourceName = "dummy source";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_advertisement);
//
//        wsManager = new WsManager(this);
//        data = new HashMap<String, String>();
//
//        imageView = (ImageView) findViewById(R.id.imgSelectedAdd);
//        btnSelectImage = (Button) findViewById(R.id.btnSelectImage);
//        btnUploadImage = (Button) findViewById(R.id.btnUploadAdd);
//        editTitle = (TextInputLayout) findViewById(R.id.inputAddTitle);
//        editDescription = (TextInputLayout) findViewById(R.id.inputAddDescription);
//
//        btnSelectImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), 1);
//
//            }
//        });
//
//        btnUploadImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
//                    convetImageToString();
//                    data.put("title", editTitle.getEditText().getText().toString());
//                    data.put("description", editDescription.getEditText().getText().toString());
//                    data.put("source", sourceName);
//                    data.put("timestamp", getTodayDateString());
//                    data.put("image", image);
//
//                    Log.d("data is ", data.toString());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                wsManager.postAsMap(data, "http://192.168.15.90:8086/dn/insert/insert_advertisement.php", new WSResponse() {
//                    @Override
//                    public void onError(String error) {
//                        super.onError(error);
//                    }
//
//                    @Override
//                    public void onSuccess(JSONObject data) {
//
//                         Log.d("response",data.toString());
//                    }
//                });
//            }
//        });
//    }
//
//    @Override
//    protected void onActivityResult(int RC, int RQC, Intent I) {
//
//        super.onActivityResult(RC, RQC, I);
//
//        if (RC == 1 && RQC == RESULT_OK && I != null && I.getData() != null) {
//
//            Uri uri = I.getData();
//
//            try {
//
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//                imageView.setImageBitmap(bitmap);
//
//            } catch (IOException e) {
//
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    private Date today() {
//        final Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, 0);
//        return cal.getTime();
//    }
//
//
//    private String getTodayDateString() {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return dateFormat.format(today());
//    }
//
//    private void convetImageToString() {
//        ByteArrayOutputStream byteArrayOutputStreamObject;
//
//        byteArrayOutputStreamObject = new ByteArrayOutputStream();
//
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStreamObject);
//
//        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
//
//        image = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);
//
//    }
//
//    private void convertImageFromString() {
//        byte[] decodedString = Base64.decode("/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\n" +
//                "AQEBAQEBAQEBAQEBAQEBAQE", Base64.DEFAULT);
//
//        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//    }
//}
//
