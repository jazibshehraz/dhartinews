package android.dhartinews.Login.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.dhartinews.Advertiser.AdvertisementActivity;
import android.dhartinews.Auther.AutherActivity;
import android.dhartinews.Home.HomeTabedActivity;
import android.dhartinews.Login.Classes.UserDataClass;
import android.dhartinews.R;
import android.dhartinews.Reporter.ReporterActivity;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.Loading;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginFragment extends Fragment {
    EditText email,password;
    Button login;
    boolean valid = true;
    private WsManager wsManager;
    List<UserDataClass> mData = new ArrayList<UserDataClass>();      // converting UserDataClass into ArrayList
    private HashMap<String, String> data;
    private String url = Url.SERVER_URL  + "/dn/registration/login.php";
    private String userType = "";
    private String userName = "";
    private boolean userStatus = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        wsManager = new WsManager(context);
        data = new HashMap<String, String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        email=(EditText)view.findViewById(R.id.email);          // fetching all views
        password=(EditText)view.findViewById(R.id.password1);
        login=(Button) view.findViewById(R.id.login);

       // email.setText("abc@example.com");
        password.setText("123456");

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validation()){
                    try {

                        data.put("email",email.getText().toString());
                        data.put("password",password.getText().toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    wsManager.postAsMap(data, url, new WSResponse() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onSuccess(JSONObject data) {
                            if(data.optString("status")=="true"){
                                parseResponse(data);
                            }
                            else {
                                Toast.makeText(getActivity(),"Invalid Username or password or user is blocked",Toast.LENGTH_LONG).show();
                            }

                            Log.d("Response is ",data.toString());

                        }

                        @Override
                        public void onError(String error) {

                            Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
                        }


                    }
                    );

                }
                else{
                    valid=false;
                }

            }

        });
    }

    private void parseResponse(JSONObject data)
    {
        try {
            JSONArray array = data.getJSONArray("user");
            JSONObject user = array.getJSONObject(0);
            userType = user.getString("usertype");
            userName = user.getString("username");
            userStatus = Boolean.valueOf(user.getString("status"));

           if(userType.equals("Admin"))
           {
               Intent intent = new Intent(getActivity(),HomeTabedActivity.class);
               intent.putExtra("USER_TYPE","admin");
               intent.putExtra("USER_NAME",userName);
               startActivity(intent);
           }

            if(userType.equals("Auther"))
            {
                Intent intent = new Intent(getActivity(),AutherActivity.class);
                startActivity(intent);
            }

            if(userType.equals("Reporter"))
            {
                Intent intent = new Intent(getActivity(),ReporterActivity.class);
                startActivity(intent);
            }

            if(userType.equals("User"))
            {
                Intent intent = new Intent(getActivity(),HomeTabedActivity.class);
                intent.putExtra("USER_TYPE","user");
                intent.putExtra("USER_NAME",userName);
                startActivity(intent);
            }

            if(userType.equals("Advertiser"))
            {
                Intent intent = new Intent(getActivity(),AdvertisementActivity.class);
                startActivity(intent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private boolean validation(){

        final String email_string = email.getText().toString().trim();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!email_string.matches(emailPattern) || email_string.isEmpty())
        {
            email.setError("invalid email pattern");
            //valid=false;

            return false;
        }

        String pass = password.getText().toString().trim();
        if(pass.isEmpty() || pass.length()<5){
            password.setError("password length min is 6");
            password.requestFocus();
           // valid=false;
            return false;
        }

        return true;

    }
}




//import android.content.Context;
//import android.content.Intent;
//import android.dhartinews.Admin.ViewUserActivity;
//import android.dhartinews.Login.Classes.UserDataClass;
//import android.dhartinews.R;
//import android.dhartinews.Variable.Url;
//import android.dhartinews.WsManager.WSResponse;
//import android.dhartinews.WsManager.WsManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//public class LoginFragment extends Fragment {
//
//    EditText email,password;
//    Button login;
//    private WsManager wsManager;
//    List<UserDataClass> mData = new ArrayList<UserDataClass>();      // converting UserDataClass into ArrayList
//    private HashMap<String, String> data;
//    private String url = Url.SERVER_URL  + "/dn/registration/login.php";
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        wsManager = new WsManager(context);
//        data = new HashMap<String, String>();
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_login, container, false);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        email=(EditText)view.findViewById(R.id.email);          // fetching all views
//        password=(EditText)view.findViewById(R.id.password1);
//        login=(Button) view.findViewById(R.id.login);
//
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {                //athentication from the user
//
//                try {
//                    data.put("email",email.getText().toString());
//                    data.put("password",password.getText().toString());
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                wsManager.postAsMap(data, url, new WSResponse() {
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onSuccess(JSONObject data) {
//                        if(data.optString("status")=="true"){
//                            Toast.makeText(getActivity(),"activity called",Toast.LENGTH_SHORT).show();
//                            Intent intent=new Intent(getActivity(),ViewUserActivity.class);
//                            startActivity(intent);
//                        }
//
//                    }
//
//                    @Override
//                    public void onError(String error) {
//                        Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
//                    }
//                });
//
//            }
//        });
//    }
//}
