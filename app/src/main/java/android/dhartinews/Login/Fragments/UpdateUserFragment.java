package android.dhartinews.Login.Fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.dhartinews.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Mirza Shafique on 13/11/2017.
 */

public class UpdateUserFragment extends DialogFragment {
    Button cancel, update;
    EditText username, email, password;
    Spinner spinner;
    UpdateCommunicator updateCommunicator;
    View view;
    boolean valid = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dialog_update, container, false);
        cancel = (Button) view.findViewById(R.id.cancel);
        username = (EditText) view.findViewById(R.id.username_update);
        update = (Button) view.findViewById(R.id.update);
        email = (EditText) view.findViewById(R.id.email_up);
        password = (EditText) view.findViewById(R.id.passeord_up);
        spinner = (Spinner) view.findViewById(R.id.spinner_update);
        ArrayList<String> entries = new ArrayList<String>();
        entries.add("User");
        entries.add("Repoter");
        entries.add("Auther");    //simple_spinner_item
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, entries);
        // adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();
                if (valid == true) {
                    updateCommunicator.updateResponse(username.getText().toString(), email.getText().toString(), password.getText().toString(), spinner.getSelectedItem().toString());
                    dismiss();

                } else {
                    valid = false;
                }
            }
        });

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            updateCommunicator = (UpdateCommunicator) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    public interface UpdateCommunicator {
        public void updateResponse(String username, String email, String password, String usertype);
    }

    public void validation() {
        final String email_string = email.getText().toString().trim();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";             // email pattren: mirza121@gmail.com
        final String namePattern = "[a-zA-Z]+";                                     // user connot enter username in the form of numbers or empty
        if (!email_string.matches(emailPattern) || email_string.isEmpty()) {
            email.setError("invalid email pattern");
            valid = false;
        }
        String name = username.getText().toString().trim();
        if (name.isEmpty() || !name.matches(namePattern)) {
            username.setError("plz enter valid user name");
            valid = false;
        }
        String pass = password.getText().toString().trim();
        if (pass.isEmpty() || pass.length() < 6) {
            password.setError("password length min is 6");
            password.requestFocus();
            valid = false;
        }

    }
}


//import android.app.Activity;
//import android.app.DialogFragment;
//import android.dhartinews.R;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.Toast;
//
//
//
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//
///**
// * Created by Mirza Shafique on 13/11/2017.
// */
//
//public class UpdateUserFragment extends DialogFragment  {
//    Button cancel,update;
//    EditText username,email,password;
//    Spinner spinner;
//    UpdateCommunicator updateCommunicator;
//    View view;
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        view=inflater.inflate(R.layout.fragment_dialog_update,container,false);
//        cancel=(Button)view.findViewById(R.id.cancel);
//        username=(EditText)view.findViewById(R.id.username_update);
//        update=(Button)view.findViewById(R.id.update);
//        email=(EditText)view.findViewById(R.id.email_up);
//        password=(EditText)view.findViewById(R.id.passeord_up);
//        spinner=(Spinner)view.findViewById(R.id.spinner_update);
//        ArrayList<String> entries=new ArrayList<String>();
//        entries.add("User");
//        entries.add("Repoter");
//        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,entries);
//// adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dismiss();
//            }
//        });
//        update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                updateCommunicator.updateResponse(username.getText().toString(),email.getText().toString(),password.getText().toString(),spinner.getSelectedItem().toString());
//                dismiss();
//            }
//        });
//
//        return view;
//    }
//
//
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            updateCommunicator =(UpdateCommunicator)  activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnHeadlineSelectedListener");
//        }
//
//    }
//    public interface UpdateCommunicator{
//        public void updateResponse(String username,String email,String password,String usertype);
//    }
//}
//
//
