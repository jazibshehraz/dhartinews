package android.dhartinews.Login.Fragments;

import android.dhartinews.R;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {

    private static final char ITEM_ONE = 'j';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);

        //set adapter to your ViewPager
        viewPager.setAdapter(new MyAdapterViewPager(getSupportFragmentManager()));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

}

class MyAdapterViewPager extends FragmentPagerAdapter {

    public MyAdapterViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if (position == 0) {
            fragment = new LoginFragment();
        }
        if (position == 1) {
            fragment = new RegistrationFragment();
        }
        return fragment;

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "SIGNIN";
        }
        if (position == 1) {
            return "SIGNUP";
        }
//        if (position == 2) {
//            return "Wednesday";
//        }
//        if (position == 3) {
//            return "Thursday";
//        }
//        if (position == 4) {
//            return "Friday";
//        }
//        if (position == 5) {
//            return "Saturday";
//        }
//        if (position == 6) {
//            return "Sunday";
//        }
        return null;
    }
}
