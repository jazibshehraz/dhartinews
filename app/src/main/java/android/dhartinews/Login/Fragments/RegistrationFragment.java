package android.dhartinews.Login.Fragments;


import android.content.Context;
import android.dhartinews.R;
import android.dhartinews.Variable.Url;
import android.dhartinews.WsManager.WSResponse;
import android.dhartinews.WsManager.WsManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class RegistrationFragment extends Fragment {
    EditText username,email,password,usertype;
    Button register;
    private WsManager wsManager;
    private HashMap<String,String> map;
    Spinner spinner;
    //String email_athentication_url="http://192.168.1.9/dherti_news/email_athentication.php";
   private String emailAuthenticationUrl = Url.SERVER_URL + "/dn/registration/email_authentication.php";
   private String signupUrl = Url.SERVER_URL + "/dn/registration/signup.php";
   private boolean valid=true;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        wsManager =new WsManager(context);
        map=new HashMap<String, String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_registration, container, false);
        username=(EditText)view.findViewById(R.id.username_reg);                    //fetching all views
        email=(EditText)view.findViewById(R.id.email_reg);
        password=(EditText)view.findViewById(R.id.password_reg);
        usertype=(EditText)view.findViewById(R.id.username_reg);
        register=(Button)view.findViewById(R.id.reg);
        spinner=(Spinner)view.findViewById(R.id.spinner_reg);
        ArrayList<String> entries=new ArrayList<String>();
        entries.add("User");
        entries.add("Repoter");
        entries.add("Auther");
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,entries);
// adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {// on button click, putting values in HashMap
                validation();
                if (valid == true) {
                    try {
                        map.put("username", username.getText().toString());
                        map.put("email", email.getText().toString());
                        map.put("password", password.getText().toString());
                        map.put("usertype", spinner.getSelectedItem().toString());
                        map.put("timestamp", getTodayDateString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    wsManager.postAsMap(map, emailAuthenticationUrl, new WSResponse() {
                        @Override
                        public void onSuccess(JSONObject data) {
                            try {
                                if (data.getString("status").equals("true")) {
                                    Toast.makeText(getActivity(), "Email is already exist.", Toast.LENGTH_SHORT).show();
                                } else {
                                    wsManager.postAsMap(map, signupUrl, new WSResponse() {
                                        @Override
                                        public void onSuccess(JSONObject data) {
                                            try {
                                                if (data.getString("status").equals("true")) {
                                                    Toast.makeText(getActivity(), data.getString("status"), Toast.LENGTH_SHORT).show();
                                                    ViewPager view;
                                                    view = (ViewPager) getActivity().findViewById(R.id.viewPager);
                                                    view.setCurrentItem(0, true);


                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onError(String error) {
                                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(String error) {
                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    valid=true;
                }
            }
        });

    }
    private Date today() {              //geting current date
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }
    private String getTodayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(today());
    }
    public void validation(){
        final String email_string = email.getText().toString().trim();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";             // email pattren: mirza121@gmail.com
        final String namePattern = "[a-zA-Z]+";                                     // user connot enter username in the form of numbers or empty
        if (!email_string.matches(emailPattern) || email_string.isEmpty())
        {
            email.setError("invalid email pattern");
            valid=false;
        }
        String name=username.getText().toString().trim();
        if(name.isEmpty() || !name.matches(namePattern)){
            username.setError("plz enter valid user name");
            valid=false;
        }
        String pass=password.getText().toString().trim();
        if(pass.isEmpty() || pass.length()<6){
            password.setError("password length min is 6");
            password.requestFocus();
            valid=false;
        }

    }
}



//import android.content.Context;
//import android.dhartinews.R;
//import android.dhartinews.Variable.Url;
//import android.dhartinews.WsManager.WSResponse;
//import android.dhartinews.WsManager.WsManager;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.Toast;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//
//
//public class RegistrationFragment extends Fragment {
//EditText username,email,password,usertype;
//    Button register;
//    private WsManager wsManager;
//    private HashMap<String,String> map;
//    Spinner spinner;
//    private String url = Url.SERVER_URL  + "/dn/registration/signup.php";
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        wsManager=new WsManager(context);
//        map=new HashMap<String, String>();
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view =inflater.inflate(R.layout.fragment_registration, container, false);
//        username=(EditText)view.findViewById(R.id.username_reg);                    //fetching all views
//        email=(EditText)view.findViewById(R.id.email_reg);
//        password=(EditText)view.findViewById(R.id.password_reg);
//        usertype=(EditText)view.findViewById(R.id.username_reg);
//        register=(Button)view.findViewById(R.id.reg);
//        spinner=(Spinner)view.findViewById(R.id.spinner_reg);
//        ArrayList<String> entries=new ArrayList<String>();
//        entries.add("User");
//        entries.add("Repoter");
//        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,entries);
//// adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {                        // on button click, putting values in HashMap
//                try {
//                    map.put("username",username.getText().toString());
//                    map.put("email",email.getText().toString());
//                    map.put("password",password.getText().toString());
//                    map.put("usertype",spinner.getSelectedItem().toString());
//                    map.put("timestamp",getTodayDateString());
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//                wsManager.postAsMap(map, url, new WSResponse() {
//                    @Override
//                    public void onSuccess(JSONObject data) {
//                        try {
//                            if(data.getString("status").equals("true"))
//                            {
//                               Toast.makeText(getActivity(),data.getString("status"),Toast.LENGTH_SHORT).show();
//                                ViewPager view;
//                                view=(ViewPager)getActivity().findViewById(R.id.viewPager);
//                                view.setCurrentItem(0,true);
//
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//
//                    @Override
//                    public void onError(String error) {
//                        Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        });
//
//    }
//    private Date today() {              //geting current date
//        final Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, 0);
//        return cal.getTime();
//    }
//    private String getTodayDateString() {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        return dateFormat.format(today());
//    }
//}
//
