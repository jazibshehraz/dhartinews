package android.dhartinews.Login.Classes;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Mirza Shafique on 12/10/2017.
 */

public class Message {
    public static void message(Context context, String message){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show();
    }
}
