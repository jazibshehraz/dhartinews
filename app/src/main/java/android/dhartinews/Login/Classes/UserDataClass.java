package android.dhartinews.Login.Classes;

import org.json.JSONObject;

import java.security.PublicKey;
import java.util.ArrayList;

/**
 * Created by Mirza Shafique on 11/11/2017.
 */

public class UserDataClass {          //getter setter class
   private JSONObject data;

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
