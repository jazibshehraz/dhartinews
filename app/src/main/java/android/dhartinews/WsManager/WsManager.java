package android.dhartinews.WsManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.dhartinews.R;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by jazib Shehraz on 11/4/2017.
 */

public class WsManager {

    private Context context;
    private Loading loading;

    public WsManager(Context context) {

        this.context = context;
        loading = new Loading(context);
    }

    public void post(JSONObject js, String url, final WSResponse wsResponse) {

        //showDialog("Please wait...");
        loading.show();
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, url, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("res", response.toString());
                        loading.hide();
                        wsResponse.onSuccess(response);
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error.getLocalizedMessage() != null)
                    loading.hide();
                    wsResponse.onError(error.getLocalizedMessage().toString());
            }
        });


//
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(objectRequest);

        loading.hide();
    }

    public void postAsMap(Map<String, String> map, String url, final WSResponse wsResponse) {
        loading.show();
        RequestQueue queue = Volley.newRequestQueue(context);

        // map.put("param1", "example");


        // the JSON request
        // JsonObjectRequest(METHOD, URL, JSONOBJECT(PARAMETERS), OK_LISTENER, ERROR_LISTENER);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, // the request method
                url, // the URL
                new JSONObject(map), // the parameters for the php
                new Response.Listener<JSONObject>() { // the response listener
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("res", response.toString());
                        loading.hide();
                        wsResponse.onSuccess(response);

                    }
                },
                new Response.ErrorListener() { // the error listener
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.hide();
                        wsResponse.onError(error.getLocalizedMessage());
                    }


                });

//    loading.hide();

        // executing the quere to get the json information
        queue.add(request);

    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        Picasso.with(context).load(url).placeholder(R.drawable.loading_icon).into(imageView);
    }
}
