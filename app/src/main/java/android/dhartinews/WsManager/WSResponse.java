package android.dhartinews.WsManager;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by jazib Shehraz on 11/4/2017.
 */

public abstract class WSResponse {
    private static String TAG = "WSResponse";

    abstract public void onSuccess(JSONObject data);
    public void onError(String error){
        Log.d(TAG, "Error: " + String.valueOf(error));
    };


}