package android.dhartinews.WsManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.dhartinews.R;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jazib Shehraz on 11/4/2017.
 */

public class dd {


    String TAG = "WSManager";
    //    private String baseUrl = "http://cognitivehealthcare.drillsol.com/projects/cognitivehealthcare/";
    public String baseUrl = "http://192.168.15.90:8086/dn/index.php";
    //    public String baseUrl = "http://172.20.10.9:8020/projects/just_buy/";
    private ProgressDialog progressDialog;
    private JSONObject responseObject;



//    public static void _post(Context context, String url, HashMap<String, Object> map, WSResponse response) {
//        JSONObject jsonObject = new JSONObject(map);
//      //  WSManager.getInstance()._post(context, url, jsonObject, response);
//    }

    public void post(final Context context, String url, JSONObject js, final WSResponse wsResponse) {

        showDialog(context, "Please Wait ... ");

        String serviceUrl = this.baseUrl + url;
        Log.d(TAG, "Request: " + serviceUrl);
        Log.d(TAG, "Data: " + js.toString());

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, serviceUrl, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                                wsResponse.onSuccess(response);
                                Log.d("respo",response.toString());

                            Log.d(TAG, String.valueOf(response));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        dismissDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                wsResponse.onError( error.getLocalizedMessage());
                dismissDialog();

            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                Log.d("header", String.valueOf(headers));
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjReq);
                    }

                    private void showDialog(Context context, String msg) {
                        if (progressDialog == null) {
                            progressDialog = new ProgressDialog(context);
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.setCancelable(false);
                        }

                        if (progressDialog != null) {
                            if (!progressDialog.isShowing()) {
                                progressDialog.setMessage(msg);
                                progressDialog.show();
                            }
                        }
                    }

                    private void dismissDialog() {
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                progressDialog = null;
                            }
                        }
                    }

                    //gif for image loading

                    public static void gif(Context context, String imageUrl, ImageView imageView) {
                        //Picasso.with(context).load(imageUrl).placeholder(R.drawable.image_gif).error(R.drawable.image_error_gif).into(imageView);
                    }

                }
